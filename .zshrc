# ========== WSL ==========
UNDER_WSL=0
read version < /proc/version
if [[ $version == *microsoft* || $version == *Microsoft* ]] then
  UNDER_WSL=1                  # running under WSL
  export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0.0"
  export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
#   export DISPLAY=localhost:0.0 # enable support for x forwarding
  export LIBGL_ALWAYS_INDIRECT=1
  export NO_AT_BRIDGE=1
  eval $(dbus-launch --sh-syntax)
fi

# ========== Environment ==========
export ZSH=$HOME/.oh-my-zsh        # oh-my-zsh location
export UPDATE_ZSH_DAYS=13          # days before auto update
export VISUAL=vim                  # default editor
export EDITOR=$VISUAL              # backup editor
export LESS="-RFX"                 # default less options
export GIT_PARENT_PATH="$HOME/git" # location of git projects
export GOROOT=/usr/local/go        # go root directory
export GOPATH=$HOME/go             # go home directory

# ========== PATH ==========
export set PATH=$PATH:$GOPATH/bin:$GOROOT/bin # add go to path
export set PATH=$PATH:$HOME/bin/scripts # custom home scripts

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

ZSH_THEME="best"            # shell theme

HISTFILE=~/.zsh_history # history location
HISTSIZE=1000           # history kept in session
SAVEHIST=1000           # history saved between sessions

HYPHEN_INSENSITIVE="true"      # _ and - are interchangable for completion
ENABLE_CORRECTION="true"       # command auto-correct
# COMPLETION_WAITING_DOTS="true" # red dots while waiting
# DISABLE_MAGIC_FUNCTIONS="true" # can fix URL pasting

# ========== Shell options ==========
# no duplicates in history
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS

# append to history
setopt INC_APPEND_HISTORY # append to history
setopt HIST_REDUCE_BLANKS # remove trailing blanks

setopt autocd notify      # cd to target if command can't exec
unsetopt beep             # no terminal beep

autoload -U colors && colors # color name definitions

# custom keybinds
bindkey "^N" expand-or-complete
bindkey '^?' backward-delete-char

# ========== Plugins ==========
plugins=(git)

# ========== Oh My Zsh ==========
source $ZSH/oh-my-zsh.sh

# ========== User aliases ==========
if [[ -f $HOME/.aliases ]] then
  source $HOME/.aliases
fi

if [[ -f $HOME/.ssh_aliases ]] then
  source $HOME/.ssh_aliases
fi

