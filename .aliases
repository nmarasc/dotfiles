UNAME=`uname`
if [[ "$UNAME" == 'MINGW64_NT-10.0' ]]; then
  alias python='winpty python' # fix console printing
  alias sandbox='cd ~/git/sandbox ; source Scripts/activate'
elif [[ "$UNAME" == 'Linux' ]]; then
  alias python='python3.7' # python3 default
  alias pip='pip3'       # pip3 default
  alias sandbox='cd ~/git/sandbox ; source bin/activate'
fi

# color support
if [ -x /usr/bin/dircolors ]; then
  alias ls='ls --color=auto'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'

  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# general shortcuts
alias q='exit'
alias c='clear'
alias h='history'
alias la='ls -A'
alias ll='ls -l'
alias cs='clear;ls'
alias p='cat'
alias t='time'
alias k='kill'
alias vi='vi -p'     # open multiple in tabs
alias tmux='tmux -2' # force 256 color mode

alias vimrc='vi ~/.vimrc'
alias zshrc='vi ~/.zshrc'
alias bashrc='vi ~/.bashrc'
alias aliases='vi ~/.aliases'

# java
alias jcom='javac *.java'
alias jcln='rm *.class'

# git
alias st='git status'
alias lg='git log'
alias clone='git clone'
alias groot="cd `git rev-parse --show-toplevel`"
alias grank="git ls-files | xargs -n1 git blame --line-porcelain | sed -n 's/^author //p' | sort -f | uniq -ic | sort -nr"
alias rst="find $GIT_PARENT_PATH -name .git -execdir pwd \; -execdir git -c color.status=always status -s \; -execdir echo \;"
alias pushHeroku='git push;git push heroku'

# ffmpeg
palgen(){
  ffmpeg -i "$1" -filter_complex "[0:v] palettegen" palette.png
}
paluse(){
  ffmpeg -i "$1" -i palette.png -filter_complex "[0:v][1:v] paletteuse" ${1%.mp4}.gif
}
makegif(){
  palgen "$1"
  paluse "$1"
  rm palette.png
}
